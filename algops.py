from twisted.internet import task
from twisted.internet import reactor
from twisted.internet.protocol import DatagramProtocol
from numba import njit
from numba.typed import List
import argparse
import dpkt

NUM_OF_COL = 192
NUM_OF_ROW = 56


@njit  
def init_empty(num_of_trx):
    tmp = List()
    for _ in range(num_of_trx):
        l = List()
        for _ in range(NUM_OF_ROW):
            ll = List()
            for _ in range(NUM_OF_COL):
                ll.append(65535.0)
            l.append(ll)
        tmp.append(l)
    return tmp

@njit  
def init_db(depth, num_of_trx):
    tmp = List()
    for _ in range(depth):
        tmp.append(init_empty(num_of_trx=num_of_trx))
    return tmp


@njit  
def init_image_done_array(num_of_trx):
    tmp = List()
    for _ in range(num_of_trx):
        tmp.append(0)
    return tmp


@njit  
def filter(factory, row, col, tof, depth, thresh, limit, db):
    miss = 0
    for i in range(0, depth):
        if row == 25 and col == 77 and factory == 1:
            print(i, tof, db[i][factory][row][col], len(db))
        if abs(tof - db[i][factory][row][col]) >= limit:
            miss += 1
            if miss > thresh:
                break
    if row == 25 and col == 77 and factory == 1:
        print(miss)
    return miss


@njit  
def pkt_parse(data, num_of_trx, prefix, thresh, db, frame, depth, limit):
    lo = prefix
    hi = lo + num_of_trx

    if int(data[0]) < lo or int(data[0]) > hi:
        return False, -1

    factory = int(data[0]) - lo
    image_done = int(data[1]) & 128 == 128
    idx = 8
    for _ in range(128):
        row = int(data[idx + 1])
        col = int(data[idx + 3])
        tof = int(data[idx + 4] * 256 + data[idx + 5])
        db[frame][factory][row][col] = tof >> 4
        if frame == depth - 1:
            miss = filter(factory, row, col, tof >> 4, depth, thresh, limit, db)
            if miss > thresh:
                data[idx + 4] = 0xFF
                data[idx + 5] = 0xB0
        idx += 8
    return image_done, factory


@njit  
def on_end_of_frame(
    image_done, image_done_array, num_of_trx, factory, frame, depth, db
):
    if image_done:
        image_done_array[factory] = 1
        all_done = True
        for trx in range(num_of_trx):
            if image_done_array[trx] == 0:
                all_done = False
                break
        if all_done is True:
            for trx in range(num_of_trx):
                image_done_array[trx] = 0
            if frame == depth - 1:
                db.append(init_empty(num_of_trx=num_of_trx))
                db.pop(0)
            else:
                frame += 1
    return frame, image_done_array


class Filter(DatagramProtocol):
    def __init__(self, arg):
        self.depth = arg.depth + 1
        self.thresh = arg.thresh
        self.limit = arg.limit
        self.prefix = arg.prefix
        self.num_of_trx = arg.num_of_trx
        self.pcap = arg.pcap
        self.bypass = arg.bypass
        self.datagrams = []

        self.frame = 0
        self.db = List()
        self.image_done = False
        self.factory = 0
        self.image_done_array = List()

        self.pkt_cnt = 0

        if self.pcap:
            print("prepare pcap for replay...")
            with open(self.pcap, "rb") as f:
                pcap = dpkt.pcap.Reader(f)
                self.extractUdpPayload(pcap)

        self.databaseInit()
        self.imageDoneArrayInit()

        if self.bypass:
            print("bypass denoising...")
        else:
            print("denoising...")

    def databaseInit(self):
        self.db = init_db(self.depth, self.num_of_trx)

    def imageDoneArrayInit(self):
        self.image_done_array = init_image_done_array(self.num_of_trx)

    def datagramReceived(self, data, addr):
        data = bytearray(data)
        if not self.bypass:
            self.image_done, self.factory = pkt_parse(
                data,
                self.num_of_trx,
                self.prefix,
                self.thresh,
                self.db,
                self.frame,
                self.depth,
                self.limit,
            )
            self.pkt_cnt += 1
            # print(self.pkt_cnt)
        self.transport.write(data, ("127.0.0.1", 5555))

        if not self.bypass:
            self.frame, self.image_done_array = on_end_of_frame(
                self.image_done,
                self.image_done_array,
                self.num_of_trx,
                self.factory,
                self.frame,
                self.depth,
                self.db,
            )

    def startProtocol(self):
        if self.pcap:
            l = task.LoopingCall(self.sendDatagram)
            l.start(0.001)

    def sendDatagram(self):
        datagram = self.datagrams.pop(0)
        self.datagrams.append(datagram)
        self.transport.write(datagram, ("127.0.0.1", 8888))

    def extractUdpPayload(self, pcap):
        for timestamp, buf in pcap:
            str(timestamp)
            eth = dpkt.ethernet.Ethernet(buf)
            if not isinstance(eth.data, dpkt.ip.IP):
                continue
            ip = eth.data
            if isinstance(ip.data, dpkt.udp.UDP):
                udp = ip.data
                if udp.dport != 8888:
                    continue
                self.datagrams.append(udp.data)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--depth",
        type=int,
        default=4,
        help="depth (how many frames) for denoising algorithm, default=4",
    )
    parser.add_argument(
        "-t",
        "--thresh",
        type=int,
        default=2,
        help="threshold (how many misses) for denoising algorithm, default=2",
    )
    parser.add_argument(
        "-l",
        "--limit",
        type=int,
        default=16,
        help="limit (tof diff in ns) for denoising algorithm, default=16",
    )
    parser.add_argument(
        "-n",
        "--num_of_trx",
        type=int,
        default=2,
        help="number of trx, default=2",
    )
    parser.add_argument(
        "-p",
        "--pcap",
        default="40.pcap",
        help="run from pcap file",
    )
    parser.add_argument(
        "--prefix",
        type=lambda x: int(x, 0),
        default=0xBC,
        help="stream prefix (factory byte), default=0xBC",
    )
    parser.add_argument(
        "--bypass",
        action="store_true",
        help="bypass denoising filter, default dont bypass denoising filter",
    )

    arg = parser.parse_args()

    reactor.listenUDP(8888, Filter(arg))
    reactor.run()


if __name__ == "__main__":
    main()