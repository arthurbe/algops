

I use python 3.7.0 please make sure you run the following commands when 3.7.0 is first on your path!

python -m venv .env

.env\Scripts\activate.bat 

python -m pip install --upgrade pip 

pip install -r requirements.txt


after installing requirements run:

py algops.py

you shell see the following prints:

.....

0 307 354.0 5

1 307 349.0 5

2 307 348.0 5

3

0 307 349.0 5

1 307 348.0 5

2 307 352.0 5

3

0 307 348.0 5

1 307 352.0 5

2 307 348.0 5

3

0 307 352.0 5

1 307 348.0 5

2 307 353.0 5

3

0 308 348.0 5

1 308 353.0 5

2 308 352.0 5

3

..........


now just comment out all @njit decorators (# @njit) and run you shell see the following prints:

.....

0 307 306.0 5

1 307 307.0 5

2 307 306.0 5

3 307 310.0 5

4 307 307.0 5

0

0 307 307.0 5

1 307 306.0 5

2 307 310.0 5

3 307 307.0 5

4 307 307.0 5

0

0 306 306.0 5

1 306 310.0 5

2 306 307.0 5

3 306 307.0 5

4 306 306.0 5

0

0 307 310.0 5

1 307 307.0 5

2 307 307.0 5

3 307 306.0 5

4 307 307.0 5

0

0 308 307.0 5

1 308 307.0 5

2 308 306.0 5

3 308 307.0 5

4 308 308.0 5

0

....

